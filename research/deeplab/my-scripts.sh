#!/bin/bash
#
# This script is used to run local test on PASCAL VOC 2012. Users could also
# modify from this script for their use case.
#
# Usage:
#   # From the tensorflow/models/research/deeplab directory.
#   sh ./local_test.sh
#
#

# Which Dataset
# Which Model


# ******************************************************************************
# RUN ONCE ONLY
# ******************************************************************************
# From tensorflow/models/research

# Set up the working environment.
CURRENT_DIR=$(pwd)
WORK_DIR="${CURRENT_DIR}/deeplab"

# Run model_test first to make sure the PYTHONPATH is correctly set.
python "${WORK_DIR}"/model_test.py -v

# **************** TO BE ENSURED BY USER **********************
# Go to datasets folder and build the shard files
# Dataset Directory Structure
# |- dataset_seg
#   |- images
#   | |- 0001.jpg
#   | |- 0002.jpg
#   | |- ...
#   |- labels
#   | |- 0001.png
#   | |- 0002.png
#   | |- ...
#   |- lists
#   | |- train.txt
#   | |- val.txt

DATASET_DIR="datasets"
cd "${WORK_DIR}/${DATASET_DIR}"
DATA_DIR="03cotton-231-57-list6-4"
mkdir -p "${DATA_DIR}"
DATA_LOC="/home/drishti/workspace/Segmentation-Datasets-v2/scripts/models/research/03cotton-231-57-list6-4"
IMAGES="${DATA_LOC}/images"
LABELS="${DATA_LOC}/labels"
LISTS="${DATA_LOC}/list"
OUTPUT_DIR="${DATA_DIR}/tfrecord"
mkdir -p "${OUTPUT_DIR}"

echo "Converting the dataset..."
python ./build_voc2012_data.py \
  --image_folder="${IMAGES}" \
  --semantic_segmentation_folder="${LABELS}" \
  --list_folder="${LISTS}" \
  --image_format="jpg" \
  --output_dir="${OUTPUT_DIR}"

# Go back to original directory.
cd "${CURRENT_DIR}"

# Set up the working directories.
EXP_FOLDER="exp/train_on_trainval_set"
INIT_FOLDER="${WORK_DIR}/${DATASET_DIR}/${DATA_DIR}/init_models"
TRAIN_LOGDIR="${WORK_DIR}/${DATASET_DIR}/${DATA_DIR}/${EXP_FOLDER}/train"
EVAL_LOGDIR="${WORK_DIR}/${DATASET_DIR}/${DATA_DIR}/${EXP_FOLDER}/eval"
VIS_LOGDIR="${WORK_DIR}/${DATASET_DIR}/${DATA_DIR}/${EXP_FOLDER}/vis"
EXPORT_DIR="${WORK_DIR}/${DATASET_DIR}/${DATA_DIR}/${EXP_FOLDER}/export"
mkdir -p "${INIT_FOLDER}"
mkdir -p "${TRAIN_LOGDIR}"
mkdir -p "${EVAL_LOGDIR}"
mkdir -p "${VIS_LOGDIR}"
mkdir -p "${EXPORT_DIR}"

# Copy locally the trained checkpoint as the initial checkpoint.
TF_INIT_ROOT="http://download.tensorflow.org/models"
TF_INIT_CKPT="deeplabv3_mnv2_pascal_train_aug_2018_01_29.tar.gz"
cd "${INIT_FOLDER}"
wget -nd -c "${TF_INIT_ROOT}/${TF_INIT_CKPT}"
tar -xf "${TF_INIT_CKPT}"
cd "${CURRENT_DIR}"

DATASET="${WORK_DIR}/${DATASET_DIR}/${DATA_DIR}/tfrecord"


# ******************************************************************************
# ------------------------------------------------------------------------------
# ******************************************************************************
# RUN IN EVERY NEW TERMINAL
# ******************************************************************************

# From tensorflow/models/research

# Set up the working environment.
CURRENT_DIR=$(pwd)
WORK_DIR="${CURRENT_DIR}/deeplab"

# Run model_test first to make sure the PYTHONPATH is correctly set.
python "${WORK_DIR}"/model_test.py -v

DATASET_DIR="datasets"
cd "${WORK_DIR}/${DATASET_DIR}"
DATA_DIR="03cotton-231-57-list6-4"
DATA_LOC="/home/drishti/workspace/Segmentation-Datasets-v2/scripts/models/research/03cotton-231-57-list6-4"
IMAGES="${DATA_LOC}/images"
LABELS="${DATA_LOC}/labels"
LISTS="${DATA_LOC}/list"
OUTPUT_DIR="${DATA_DIR}/tfrecord"

# Go back to original directory.
cd "${CURRENT_DIR}"

# Set up the working directories.
EXP_FOLDER="exp/train_on_trainval_set"
INIT_FOLDER="${WORK_DIR}/${DATASET_DIR}/${DATA_DIR}/init_models"
TRAIN_LOGDIR="${WORK_DIR}/${DATASET_DIR}/${DATA_DIR}/${EXP_FOLDER}/train"
EVAL_LOGDIR="${WORK_DIR}/${DATASET_DIR}/${DATA_DIR}/${EXP_FOLDER}/eval"
VIS_LOGDIR="${WORK_DIR}/${DATASET_DIR}/${DATA_DIR}/${EXP_FOLDER}/vis"
EXPORT_DIR="${WORK_DIR}/${DATASET_DIR}/${DATA_DIR}/${EXP_FOLDER}/export"

# Copy locally the trained checkpoint as the initial checkpoint.
TF_INIT_ROOT="http://download.tensorflow.org/models"
TF_INIT_CKPT="deeplabv3_pascal_train_aug_2018_01_04.tar.gz"

DATASET="${WORK_DIR}/${DATASET_DIR}/${DATA_DIR}/tfrecord"

# ******************************************************************************
# ------------------------------------------------------------------------------
# ******************************************************************************
# RUN THE FOLLOWING FOR TRAINING, EVALUATION, VISUALIATION, AND EXPORTING MODEL
# ******************************************************************************


# Train 10 iterations.
NUM_ITERATIONS=10
python "${WORK_DIR}"/train_m.py \
  --logtostderr \
  --train_split="train" \
  --model_variant="xception_65" \
  --atrous_rates=6 \
  --atrous_rates=12 \
  --atrous_rates=18 \
  --output_stride=16 \
  --decoder_output_stride=4 \
  --train_crop_size=513 \
  --train_crop_size=513 \
  --train_batch_size=4 \
  --training_number_of_steps="${NUM_ITERATIONS}" \
  --fine_tune_batch_norm=true \
  --tf_initial_checkpoint="${INIT_FOLDER}/deeplabv3_pascal_train_aug/model.ckpt" \
  --train_logdir="${TRAIN_LOGDIR}" \
  --dataset_dir="${DATASET}" \
  --dataset="dataset_seg" \
  --initialize_last_layer = False \
  --last_layers_contain_logits_only = False

python "${WORK_DIR}"/train.py \
  --logtostderr \
  --train_split="train" \
  --model_variant="mobilenet_v2" \
  --output_stride=16 \
  --train_crop_size=513 \
  --train_crop_size=513 \
  --train_batch_size=4 \
  --training_number_of_steps="${NUM_ITERATIONS}" \
  --fine_tune_batch_norm=true \
  --tf_initial_checkpoint="${INIT_FOLDER}/${CKPT_NAME}deeplabv3_mnv2_pascal_train_aug/model.ckpt-30000" \
  --train_logdir="${TRAIN_LOGDIR}" \
  --dataset_dir="${DATASET}" \
  --dataset="03cotton-231-57-list6-4" \
  --initialize_last_layer = False \
  --last_layers_contain_logits_only = False

# ..............................................................................

# Run evaluation. This performs eval over the full val split (1449 images) and
# will take a while.
# Using the provided checkpoint, one should expect mIOU=82.20%.
python "${WORK_DIR}"/eval_m.py \
  --logtostderr \
  --eval_split="val" \
  --model_variant="xception_65" \
  --atrous_rates=6 \
  --atrous_rates=12 \
  --atrous_rates=18 \
  --output_stride=16 \
  --decoder_output_stride=4 \
  --eval_crop_size=720 \
  --eval_crop_size=1280 \
  --checkpoint_dir="${TRAIN_LOGDIR}" \
  --eval_logdir="${EVAL_LOGDIR}" \
  --dataset_dir="${DATASET}" \
  --max_number_of_evaluations=1 \
  --dataset="dataset_seg"

python "${WORK_DIR}"/eval.py \
  --logtostderr \
  --eval_split="val" \
  --model_variant="mobilenet_v2" \
  --eval_crop_size=720 \
  --eval_crop_size=1280 \
  --checkpoint_dir="${TRAIN_LOGDIR}" \
  --eval_logdir="${EVAL_LOGDIR}" \
  --max_number_of_evaluations=1 \
  --dataset_dir="${DATASET}" \
  --dataset="03cotton-231-57-list6-4" \
  --initialize_last_layer = False \
  --last_layers_contain_logits_only = False

# ..............................................................................

# Visualize the results.
python "${WORK_DIR}"/vis_m.py \
  --logtostderr \
  --vis_split="val" \
  --model_variant="xception_65" \
  --atrous_rates=6 \
  --atrous_rates=12 \
  --atrous_rates=18 \
  --output_stride=16 \
  --decoder_output_stride=4 \
  --vis_crop_size=720 \
  --vis_crop_size=1280 \
  --checkpoint_dir="${TRAIN_LOGDIR}" \
  --vis_logdir="${VIS_LOGDIR}" \
  --dataset_dir="${DATASET}" \
  --max_number_of_iterations=1 \
  --dataset="dataset_seg"

python "${WORK_DIR}"/vis.py \
  --logtostderr \
  --vis_split="val" \
  --model_variant="mobilenet_v2" \
  --vis_crop_size=720 \
  --vis_crop_size=1280 \
  --checkpoint_dir="${TRAIN_LOGDIR}" \
  --vis_logdir="${VIS_LOGDIR}" \
  --dataset_dir="${DATASET}" \
  --max_number_of_iterations=1 \
  --dataset="03cotton-231-57-list6-4" \
  --initialize_last_layer = False \
  --last_layers_contain_logits_only = False

# ..............................................................................

# Export the trained checkpoint.
CKPT_PATH="${TRAIN_LOGDIR}/model.ckpt-${NUM_ITERATIONS}"
EXPORT_PATH="${EXPORT_DIR}/frozen_inference_graph.pb"

python "${WORK_DIR}"/export_model_m.py \
  --logtostderr \
  --checkpoint_path="${CKPT_PATH}" \
  --export_path="${EXPORT_PATH}" \
  --model_variant="xception_65" \
  --atrous_rates=6 \
  --atrous_rates=12 \
  --atrous_rates=18 \
  --output_stride=16 \
  --decoder_output_stride=4 \
  --num_classes=7 \
  --crop_size=513 \
  --crop_size=513 \
  --inference_scales=1.0

NUM_ITERATIONS=11496
CKPT_PATH="${TRAIN_LOGDIR}/model.ckpt-${NUM_ITERATIONS}"
EXPORT_PATH="${EXPORT_DIR}/frozen_inference_graph.pb"

python "${WORK_DIR}"/export_model.py \
  --logtostderr \
  --checkpoint_path="${CKPT_PATH}" \
  --export_path="${EXPORT_PATH}" \
  --model_variant="mobilenet_v2" \
  --num_classes=4 \
  --crop_size=513 \
  --crop_size=513 \
  --inference_scales=1.0 \
  --dataset="03cotton-231-57-list6-4"

# Run inference with the exported checkpoint.
# Please refer to the provided deeplab_demo.ipynb for an example.

